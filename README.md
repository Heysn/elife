------README------

**XAMPP og phpMyAdmin er blitt brukt til lokal kjøring og testing**

1. Endre navn på mappen "elife-master" til "elife"
2. Opprett/importer databasefilen "elifedb.sql" i phpMyAdmin
3. Gå til localhost/elife/public_html i nettleseren
4. Det skal dukke opp en login side. Logg på med brukernavn "test", passord "321"
5. Trykk på "Gå til CRM"

På dashbordet kan man legge til, endre, og slette kategorier, produkter, kunder.
Også mulighet for å opprette et salg knyttet mot mobilnummeret til en registrert kunde.