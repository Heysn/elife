<?php

class Database
{
    private $conn;

    public function connect(){
      include_once("constants.php");
      $this->conn = new mysqli(host, user, pass, db);
      if ($this->conn) {
        //echo må være over return
        //echo "tilkoblet";
        return $this->conn;
      }
      return "Databaseforbindelse kunne ikke oppnås.";
    }
}

//$db = new Database();
//$db->connect();

?>
