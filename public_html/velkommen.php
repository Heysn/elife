<?php
  include_once './db/constants.php';
  if (!isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/");
  }
 ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Velkommen!</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" rel="stylesheet" href="./includes/style.css">
    <script type="text/javascript" src="./js/main.js"></script>
    <div class="overlay"><div class="loader"></div></div>
</head>
<body>

  <?php include_once './templates/header.php'; ?>
  <br/><br/>
  <div class="container">
    <div class="card mx-auto" style="width: 40rem;">
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">CMS</h5>
              <p class="card-text">Her vil du kunne oppdatere og vedlikeholde nettsiden din gjennom CMS-systemet.</p>
              <a href="#" class="btn btn-primary">Gå til CMS</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Lagerstyring og salg</h5>
              <p class="card-text">Hold styr på lageret og salgsoversikten din ved å gå til ditt CRM-system.</p>
              <a href="dashbord.php" class="btn btn-primary">Gå til CRM</a>
            </div>
          </div>
        </div>
      </div>


</body>
</html>
