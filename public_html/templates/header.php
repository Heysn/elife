<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Elife</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
        <?php
        if (isset($_SESSION['b_id'])) {
          ?>
          <li class="nav-item">
            <a class="nav-link " href="./velkommen.php"><i class="fa fa-arrow-circle-left">&nbsp;</i>CMS/CRM<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="./dashbord.php"><i class="fa fa-home">&nbsp;</i>Dashbord<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout.php"><i class="fa fa-user">&nbsp;</i>Logg av</a>
          </li>
          <?php
        }
         ?>
    </ul>
  </div>
</nav>
