<!-- Modal -->
<div class="modal fade" id="kategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Legg til kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="kategori_form" onsubmit="return false">
          <div class="form-group">
            <label>Kategorinavn</label>
            <input type="text" class="form-control" name="kategorinavn" id="kategorinavn" placeholder="F.eks sykler, låser, diverse" required/>
            <!--<small id="katError" class="form-text text-muted"></small>-->
          </div>
          <button type="submit" class="btn btn-success">Lagre</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Avbryt</button>
      </div>
    </div>
  </div>
</div>
