<!-- Modal -->
<div class="modal fade" id="kundeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rediger Kunde</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="rediger_kunde_form" onsubmit="return false">
          <div class="form-group">
            <input type="hidden" name="brukerID" id="brukerID" value="<?php echo $_SESSION['b_id']; ?>">
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input type="hidden" name="kundeID" value="" id="kundeID" />
              <label>Fornavn</label>
              <input type="text" class="form-control" name="rediger_fornavn" id="rediger_fornavn" placeholder="Skriv fornavn" required/>
              <small id="frNavnError" class="form-text text-muted"></small>
            </div>
            <div class="form-group col-md-6">
              <label>Etternavn</label>
              <input type="text" class="form-control" name="etternavn" id="etternavn" placeholder="Skriv etternavn" required/>
              <small id="etNavnError" class="form-text text-muted"></small>
            </div>
          </div>
          <div class="form-group">
            <label>Adresse</label>
            <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Skriv adresse" required/>
            <small id="addError" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label>Postnummer</label>
            <input type="text" class="form-control" name="postNr" id="postNr" placeholder="Skriv postnummer" required/>
            <small id="pstError" class="form-text text-muted"></small>
          </div>
          <div class="form-group">
            <label>Telefon</label>
            <input type="text" class="form-control" name="telefon" id="telefon" placeholder="Skriv telefonnummer" required/>
            <small id="tlfError" class="form-text text-muted"></small>
          </div>
          <button type="submit" class="btn btn-success">Lagre</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Avbryt</button>
      </div>
    </div>
  </div>
</div>
