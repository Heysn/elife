<!-- Modal -->
<div class="modal fade" id="produktModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Legg til produkt</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="produkt_form" onsubmit="return false">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Dato</label>
              <input type="text" class="form-control" name="datoLagtTil" id="datoLagtTil" value="<?php echo date("Y-m-d")?>" readonly/>
            </div>
            <div class="form-group col-md-6">
              <label>Produktnavn</label>
              <input type="text" class="form-control" name="produktnavn" id="produktnavn" placeholder="Skriv produktnavn" required/>
              <small id="produktNavnError" class="form-text text-muted"></small>
            </div>
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <select class="form-control" id="velgKategori" name="velgKategori" required/>



            </select>
          </div>
          <div class="form-group">
            <label>Merke</label>
            <input type="text" class="form-control" name="merke" id="merke" placeholder="Skriv merke" required/>
          </div>
          <div class="form-group">
            <label>Pris</label>
            <input type="text" class="form-control" name="pris" id="pris" placeholder="Skriv pris" required/>
          </div>
          <div class="form-group">
            <label>Antall</label>
            <input type="text" class="form-control" name="antall" id="antall" placeholder="Skriv antall" required/>
          </div>
          <div class="form-group">
            <label>Notat</label>
            <input type="text" class="form-control" name="beskrivelse" id="beskrivelse" placeholder="Vil du legge til et lite notat?">
          </div>
          <button type="submit" class="btn btn-success">Lagre</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Avbryt</button>
      </div>
    </div>
  </div>
</div>
