<?php
  include_once './db/constants.php';
  if (!isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/");
  }
 ?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="./js/administrer.js"></script>
</head>
<body>

    <!--navbar-->
    <?php include_once './templates/header.php'; ?>
    <br/><br/>
    <div class="container">
      <table class="table table-hover table-bordered">
        <h2>Ordre</h2>
        <thead>
          <tr>
            <th>ListeNr</th>
            <th>OrdreNr</th>
            <th>Dato</th>
            <th>Kunde Telefon</th>
            <th>Ink.Mva.</th>
            <th>Mva</th>
            <th>Eks.Mva.</th>
            <th>Betalt</th>
            <th>Rest</th>
            <th>Metode</th>
          </tr>
        </thead>
        <tbody id="get_salg_ordre">

          <!--<tr>
            <td>1</td>
            <td>38</td>
            <td>2018-05-18</td>
            <td>12345678</td>
            <td>400</td>
            <td>100</td>
            <td>500</td>
            <td>500</td>
            <td>0</td>
            <td>Kontant</td>
          </tr>-->

          <tr>
        </tbody>
      </table>

      <p></p>

      <table class="table table-hover table-bordered">
        <h2>Ordre Detaljer</h2>
        <thead>
          <tr>
            <th>ListeNr</th>
            <th>OrdreNr</th>
            <th>Produktnavn</th>
            <th>Antall</th>
            <th>Produktpris Pr</th>
          </tr>
        </thead>
        <tbody id="get_salg_detaljer">

          <!--<tr>
            <td>1</td>
            <td>38</td>
            <td>testen</td>
            <td>1</td>
            <td>400</td>
          </tr>-->

          <tr>
        </tbody>
      </table>
    </div>

</body>
</html>
