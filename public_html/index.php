<?php
  include_once './db/constants.php';
  if (isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/dashbord.php");
  }
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" rel="stylesheet" href="./includes/style.css">
    <script type="text/javascript" src="./js/main.js"></script>
    <div class="overlay"><div class="loader"></div></div>
</head>
<body>
      <!--navbar-->
    <?php include_once './templates/header.php'; ?>
    <br/><br/>
    <div class="container">
      <div class="card mx-auto" style="width: 20rem;">
        <img class="card-img-top mx-auto" style="width: 40%;" src="./images/userlogin.png" alt="login icon">
        <div class="card-body">
          <h5 class="card-title">Login</h5>
          <form id="form_login" onsubmit="return false">
            <small id="log_error" class="form-text text-muted"></small>
            <div class="form-group">
              <label for="exampleInputUsername1">Brukernavn</label>
              <input type="text" class="form-control" name="log_bruker" id="log_bruker" placeholder="Brukernavn">
              <small id="b_error" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Passord</label>
              <input type="password" class="form-control" name="log_pass" id="log_pass" placeholder="Passord">
              <small id="p_error" class="form-text text-muted"></small>
            </div>
            <div class="row align-items-center justify-content-center">
              <button type="submit" class="btn btn-primary"><i class="fa fa-lock">&nbsp;</i>Logg Inn</button>
            </div>
          </form>
        </div>
      </div>
    </div>

</body>
</html>
