<?php

class DbO {

  private $conn;

  function __construct() {
    include_once '../db/db.php';
    $db = new Database();
    $this->conn = $db->connect();
  }

  //funksjon for å legge til ny kategori som blir sendt
  public function nyKategori($kategori) {
    $stmt = $this->conn->prepare("INSERT INTO `kategori`(`kategorinavn`, `status`) VALUES (?,?)");
    $status = 1;
    $stmt->bind_param("si", $kategori, $status);
    $result = $stmt->execute() or die ($this->conn->error);
    if ($result) {
      return "Kategori lagt til.";
    } else {
      return false;
    }
  }

  public function nyttProdukt($kategoriID, $produktnavn, $merke, $beskrivelse, $pris, $antall, $datoLagtTil) {
    $stmt = $this->conn->prepare("INSERT INTO `produkt`(`kategoriID`, `produktnavn`, `merke`, `beskrivelse`, `pris`, `antall`, `datoLagtTil`, `status`)
    VALUES (?,?,?,?,?,?,?,?)");
    $status = 1;
    $stmt->bind_param("isssdisi", $kategoriID, $produktnavn, $merke, $beskrivelse, $pris, $antall, $datoLagtTil, $status);
    $result = $stmt->execute() or die ($this->conn->error);
    if ($result) {
      return "Produkt lagt til.";
    } else {
      return false;
    }
  }

  public function getRader($table){
    $stmt = $this->conn->prepare("SELECT * FROM ".$table);
    $stmt->execute() or die($this->conn->error);
    $result = $stmt->get_result();
    $rows = array();
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()){
        $rows[] = $row;
      }
      return $rows;
    }
    return "Ingen data.";
  }

  public function nyKunde($brukerID, $fornavn, $etternavn, $adresse, $postNr, $telefon) {
    $stmt = $this->conn->prepare("INSERT INTO `kunde`(`brukerID`, `fornavn`, `etternavn`, `adresse`, `postNr`, `telefon`)
    VALUES (?,?,?,?,?,?)");
    //$brukerID = $_SESSION['b_id'];
    $stmt->bind_param("isssss", $brukerID, $fornavn, $etternavn, $adresse, $postNr, $telefon);
    $result = $stmt->execute() or die ($this->conn->error);
    if ($result) {
      return "Kunde lagt til.";
    } else {
      return false;
    }
  }
}

//$operation = new DbO();
//echo $operation->nyKunde("$_SESSION[b_id]","Jan","Johnsen","Hyllveien 12 C", "3408","12345678");
//echo $operation->nyttProdukt("7","Gazelle","Elife","hei","27000","2","2018-05-10");
//echo $operation->nyKategori("Sykler");
//echo "<pre>";
//print_r($operation->getKategorier("kategori"));

?>
