<?php

/**
* bruker class for oppretting og login
*/
class bruker {

  private $conn;

  function __construct() {
    include_once("../db/db.php");
    $db = new Database();
    $this->conn = $db->connect();
  }

  //se om email allerede er i bruk eller ikke med prepared statements
  private function emailTatt($email) {
    //en select hvor email = noe
    $stmt = $this->conn->prepare("SELECT brukerID FROM bruker WHERE email = ?");
    //spesifiserer type string for variabel email
    $stmt->bind_param("s", $email);
    $stmt->execute() or die ($this->conn->error);
    $result = $stmt->get_result();
    if($result->num_rows > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  public function lagBruker($brukernavn, $fornavn, $etternavn, $email, $passord) {
    if ($this->emailTatt($email)) {
      return "Email er enten opptatt eller ikke gyldig. Prøv igjen.";
    } else {
      //hasher passord med default siden denne konstanten endres over tid
      //da nye og bedre algoritmer blir lagt til i php
      //cost er 11 iterasjoner av algoritmen som tilsvarer default
      //bruker prepared statements for å beskytte mot sql-injection
      $hash_pwd = password_hash($passord, PASSWORD_DEFAULT);
      $sist_login = date("Y-m-d h:m:s");
      $stmt = $this->conn->prepare("INSERT INTO `bruker`(`brukernavn`, `fornavn`, `etternavn`, `email`, `passord`, `sist_login`)
      VALUES (?, ?, ?, ?, ?, ?)");
      $stmt->bind_param("ssssss", $brukernavn, $fornavn, $etternavn, $email, $hash_pwd, $sist_login);
      $result = $stmt->execute() or die($this->conn->error);
      if ($result) {
        return $this->conn->insert_id;
      } else {
        return "Kunne ikke lage bruker";
      }
    }
  }

  //notat: endret email i databasen "elifedb" til unik

  //funksjon for å logge inn bruker
  public function brukerLogin($brukernavn, $passord) {
    $stmt = $this->conn->prepare("SELECT * FROM bruker WHERE BINARY brukernavn = ?");
    $stmt->bind_param("s", $brukernavn);
    $stmt->execute() or die($this->conn->error);
    $result = $stmt->get_result();

    if ($result->num_rows < 1) {
      return "Feil ved innlogging1.";
    } else {
      $row = $result->fetch_assoc();
      if (!password_verify($passord, $row["passord"])) {
          return "Feil ved innlogging2.";
      } else {
        $_SESSION['b_id'] = $row['brukerID'];
        $_SESSION['b_navn'] = $row['brukernavn'];
        $_SESSION['b_fornavn'] = $row['fornavn'];
        $_SESSION['b_etternavn'] = $row['etternavn'];
        $_SESSION['b_email'] = $row['email'];
        $_SESSION['s_login'] = $row['sist_login'];

        //oppdaterer kolonnen sist_login når brukeren logger inn
        $sist_login = date("Y-m-d h:m:s");
        $stmt = $this->conn->prepare("UPDATE bruker SET sist_login = ? WHERE brukernavn = ?");
        $stmt->bind_param("ss", $sist_login, $brukernavn);
        $result = $stmt->execute() or die ($this->conn->error);
        if ($result) {
          return true;
        } else {
          return false;
        }
      }
    }
  }
}

//opprette bruker blir gjort under. Uncomment linje 93 og 94, så kjør filen
//tallet som dukker opp i nettleser er brukerID
//bare en bekreftelse på at det funket

//$brukeren = new bruker();
//echo $brukeren->lagBruker("test", "Testen", "Testesen", "test@hotmail.com", "321");
//echo $brukeren->lagBruker("testB", "Per", "Gundersen", "per-gun@hotmail.com", "123");
//echo $brukeren->brukerLogin("test", "4321");
//echo $_SESSION["b_navn"];
?>
