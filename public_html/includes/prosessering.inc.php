<?php
include_once("../db/constants.php");
include_once("DbO.inc.php");
include_once("bruker.inc.php");
include_once("administrer.php");

//lager et nytt objekt av klassen bruker fra bruker.inc.php og kaller funksjon brukerLogin
if (isset($_POST["log_bruker"]) AND isset($_POST["log_pass"])) {
	$brukeren = new bruker();
	$result = $brukeren->brukerLogin($_POST["log_bruker"],$_POST["log_pass"]);
	echo $result;
	exit();
}

//legg til kategori
if (isset($_POST["kategorinavn"])) {
	$kategorien = new DbO();
	$result = $kategorien->nyKategori($_POST["kategorinavn"]);
	echo $result;
	exit();
}

//henter kategorier
if (isset($_POST["getKategorier"])) {
	$obj = new DbO();
	$rows = $obj->getRader("kategori");
	foreach ($rows as $row) {
		echo "<option value='".$row["kategoriID"]."'>".$row["kategorinavn"]."</option>";
	}
	exit();
}

//legg til produkt
if (isset($_POST["datoLagtTil"]) AND isset($_POST["produktnavn"])) {
	$produktet = new DbO();
	$result = $produktet->nyttProdukt($_POST["velgKategori"], $_POST["produktnavn"], $_POST["merke"], $_POST["beskrivelse"],
																		$_POST["pris"], $_POST["antall"], $_POST["datoLagtTil"]);
	echo $result;
	exit();
}

//legg til kunde
if (isset($_POST["fornavn"]) AND isset($_POST["etternavn"])) {
	$kunden = new DbO();
	$result = $kunden->nyKunde($_POST["brukerID"], $_POST["fornavn"], $_POST["etternavn"], $_POST["adresse"],
														 $_POST["postNr"], $_POST["telefon"]);
	echo $result;
	exit();
}

//---------------administrer kategorier-----------------

//pagination og visning av kategori rader
if (isset($_POST["visKategorier"])) {
	$administrer = new Administrer();
	$result = $administrer->endreRaderPagination("kategori", $_POST["pageno"]);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if (count($rows) > 0) {
		$n = (($_POST["pageno"] - 1) * 5) + 1;
		foreach ($rows as $row) {
			?>
			<tr>
				<td><?php echo $n; ?></td>
				<td><?php echo $row["kategorinavn"]; ?></td>
				<td><a href="#" class="btn btn-success btn-sm">Aktiv</a></td>
				<td>
					<a href="#" did="<?php echo $row['kategoriID']; ?>" class="slettKategori btn btn-danger btn-sm">Slett</a>
					<a href="#" eid="<?php echo $row['kategoriID']; ?>" data-toggle="modal" data-target="#kategoriModal" class="redigerKategori btn btn-info btn-sm">Rediger</a>
				</td>
			</tr>
			<?php
			$n++;
		}
		?>
			<tr><td colspan="4"><?php echo $pagination; ?></td></tr>
		<?php
		exit();
	}
}

//mottar fra administrer.js og sletter kategorien på raden
if (isset($_POST["slettKategori"])) {
	$administrer = new Administrer();
	$result = $administrer->slettRad("kategori", "kategoriID", $_POST["id"]);
	echo $result;
}

//mottar fra administrer.js og oppdaterer data (kategorier)
if (isset($_POST["redigerKategori"])) {
	$administrer = new Administrer();
	$result = $administrer->getEnRad("kategori", "kategoriID", $_POST["id"]);
	//returnerer array og mapper inputs i form
	echo json_encode($result);
	exit();
}

//redigerer rad etter mottatt data (kategorier)
if (isset($_POST["rediger_kategorinavn"])) {
	$administrer = new Administrer();
	$id = $_POST["kategoriID"];
	$navn = $_POST["rediger_kategorinavn"];
	$result = $administrer->redigerRad("kategori", ["kategoriID"=>$id], ["kategorinavn"=>$navn, "status"=>1]);
	echo $result;
}

//---------------administrer produkter-----------------

//pagination og visning av produkt rader
if (isset($_POST["visProdukter"])) {
	$administrer = new Administrer();
	$result = $administrer->endreRaderPagination("produkt", $_POST["pageno"]);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if (count($rows) > 0) {
		$n = (($_POST["pageno"] - 1) * 5) + 1;
		foreach ($rows as $row) {
			?>
			<tr>
				<td><?php echo $n; ?></td>
				<td><?php echo $row["produktnavn"]; ?></td>
				<td><?php echo $row["kategorinavn"]; ?></td>
				<td><?php echo $row["merke"]; ?></td>
				<td><?php echo $row["beskrivelse"]; ?></td>
				<td><?php echo $row["pris"]; ?></td>
				<td><?php echo $row["antall"]; ?></td>
				<td><?php echo $row["datoLagtTil"]; ?></td>
				<td><a href="#" class="btn btn-success btn-sm">Aktiv</a></td>
				<td>
					<a href="#" did="<?php echo $row['produktID']; ?>" class="slettProdukt btn btn-danger btn-sm">Slett</a>
					<a href="#" eid="<?php echo $row['produktID']; ?>" data-toggle="modal" data-target="#produktModal" class="redigerProdukt btn btn-info btn-sm">Rediger</a>
				</td>
			</tr>
			<?php
			$n++;
		}
		?>
			<tr><td colspan="10"><?php echo $pagination; ?></td></tr>
		<?php
		exit();
	}
}

//mottar fra administrer.js og sletter produktet på raden
if (isset($_POST["slettProdukt"])) {
	$administrer = new Administrer();
	$result = $administrer->slettRad("produkt", "produktID", $_POST["id"]);
	echo $result;
}

//mottar fra administrer.js og oppdaterer data (produkter)
if (isset($_POST["redigerProdukt"])) {
	$administrer = new Administrer();
	$result = $administrer->getEnRad("produkt", "produktID", $_POST["id"]);
	//returnerer array og mapper inputs i form
	echo json_encode($result);
	exit();
}

//redigerer rad etter mottatt data (produkter)
if (isset($_POST["rediger_produktnavn"])) {
	$administrer = new Administrer();
	$id = $_POST["produktID"];
	$navn = $_POST["rediger_produktnavn"];
	$kategori = $_POST["velgKategori"];
	$merke = $_POST["merke"];
	$beskrivelse = $_POST["beskrivelse"];
	$pris = $_POST["pris"];
	$antall = $_POST["antall"];
	$dato = $_POST["datoLagtTil"];
	$result = $administrer->redigerRad("produkt", ["produktID"=>$id], ["kategoriID"=>$kategori, "produktnavn"=>$navn, "merke"=>$merke, "beskrivelse"=>$beskrivelse, "pris"=>$pris, "antall"=>$antall, "datoLagtTil"=>$dato, "status"=>1]);
	echo $result;
}

//---------------administrer kunder-----------------
//pagination og visning av kunde rader
if (isset($_POST["visKunder"])) {
	$administrer = new Administrer();
	$result = $administrer->endreRaderPagination("kunde", $_POST["pageno"]);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if (count($rows) > 0) {
		$n = (($_POST["pageno"] - 1) * 5) + 1;
		foreach ($rows as $row) {
			?>
			<tr>
				<td><?php echo $n; ?></td>
				<td><?php echo $row["brukernavn"]; ?></td>
				<td><?php echo $row["fornavn"]; ?></td>
				<td><?php echo $row["etternavn"]; ?></td>
				<td><?php echo $row["adresse"]; ?></td>
				<td><?php echo $row["postNr"]; ?></td>
				<td><?php echo $row["poststed"]; ?></td>
				<td><?php echo $row["telefon"]; ?></td>
				<td>
					<a href="#" did="<?php echo $row['kundeID']; ?>" class="slettKunde btn btn-danger btn-sm">Slett</a>
					<a href="#" eid="<?php echo $row['kundeID']; ?>" data-toggle="modal" data-target="#kundeModal" class="redigerKunde btn btn-info btn-sm">Rediger</a>
				</td>
			</tr>
			<?php
			$n++;
		}
		?>
			<tr><td colspan="9"><?php echo $pagination; ?></td></tr>
		<?php
		exit();
	}
}

//mottar fra administrer.js og sletter kunden på raden
if (isset($_POST["slettKunde"])) {
	$administrer = new Administrer();
	$result = $administrer->slettRad("kunde", "kundeID", $_POST["id"]);
	echo $result;
}

//mottar fra administrer.js og oppdaterer data (kunde)
if (isset($_POST["redigerKunde"])) {
	$administrer = new Administrer();
	$result = $administrer->getEnRad("kunde", "kundeID", $_POST["id"]);
	//returnerer array og mapper inputs i form
	echo json_encode($result);
	exit();
}

//redigerer rad etter mottatt data (produkter)
if (isset($_POST["rediger_fornavn"])) {
	$administrer = new Administrer();
	$id = $_POST["kundeID"];
	$fnavn = $_POST["rediger_fornavn"];
	$enavn = $_POST["etternavn"];
	$adresse = $_POST["adresse"];
	$postnummer = $_POST["postNr"];
	$tlf = $_POST["telefon"];
	$result = $administrer->redigerRad("kunde", ["kundeID"=>$id], ["fornavn"=>$fnavn, "etternavn"=>$enavn, "adresse"=>$adresse, "postNr"=>$postnummer, "telefon"=>$tlf]);
	echo $result;
}

//---------------Salg----------------

if (isset($_POST["getNyVare"])) {
	$dbObj = new DbO();
	$rows = $dbObj->getRader("produkt");

	?>

	<tr>
			<td><b class="nummer">1</b></td>
			<td>
				<select name="produktID[]" class="form-control form-control-sm produktID" required>
					<option value="">Velg Produkt</option>
					<?php
						foreach ($rows as $row) {
							?><option value="<?php echo $row['produktID']; ?>"><?php echo $row["produktnavn"]; ?></option><?php
						}
					?>
				</select>
			</td>
			<td><input name="antallLager[]" readonly type="text" class="form-control form-control-sm antallLager"></td>
			<td><input name="antall[]" type="text" class="form-control form-control-sm antall" required/></td>
			<td><input name="pris[]" type="text" class="form-control form-control-sm pris" readonly></span>
			<span><input name="prNavn[]" type="hidden" class="form-control form-control-sm prNavn"></td>
			<td>Kr&nbsp;<span class="total">0</span></td>

	<?php
	exit();
}

//får pris og antall fra en vare
if (isset($_POST["getPrisAntall"])) {
	$administrer = new Administrer();
	$result = $administrer->getEnRad("produkt", "produktID", $_POST["id"]);
	echo json_encode($result);
	exit();
}

//-------------- salgsprosessering -----------------
if (isset($_POST["salgsDato"]) AND isset($_POST["kundeTlf"]) AND isset($_POST["prNavn"]) AND isset($_POST["betalt"])) {
	//de med lst foran er arrays fra salgs_form
	//resten er vanlige items fra form
	$salgsDato = $_POST["salgsDato"];
	$kundeTlf = $_POST["kundeTlf"];
	$bTotal = $_POST["bruttoTotal"];
	$mva = $_POST["mva"];
	$nTotal = $_POST["nettoTotal"];
	$betalt = $_POST["betalt"];
	$rst = $_POST["resterende"];
	$mtd = $_POST["betalingsMtd"];
	$lst_prNavn = $_POST["prNavn"];
	$lst_antall = $_POST["antall"];
	$lst_pris = $_POST["pris"];
	$lst_antallLager = $_POST["antallLager"];

	$administrer = new Administrer();
	echo $result = $administrer->lagreKundeOrdre($salgsDato, $kundeTlf, $bTotal, $mva, $nTotal, $betalt, $rst, $mtd, $lst_prNavn, $lst_antall, $lst_pris, $lst_antallLager);
	exit();
}

//--------------------OVERSIKT OVER SALG/ORDRE----------------------

//pagination og visning av ORDRE rader
if (isset($_POST["visOrdre"])) {
	$administrer = new Administrer();
	$result = $administrer->endreRaderPagination("ordre", $_POST["pageno"]);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if (count($rows) > 0) {
		$n = (($_POST["pageno"] - 1) * 5) + 1;
		foreach ($rows as $row) {
			?>
			<tr>
				<td><?php echo $n; ?></td>
				<td><?php echo $row["ordreNr"]; ?></td>
				<td><?php echo $row["ordreDato"]; ?></td>
				<td><?php echo $row["telefon"]; ?></td>
				<td><?php echo $row["bruttoTotal"]; ?></td>
				<td><?php echo $row["mva"]; ?></td>
				<td><?php echo $row["nettoTotal"]; ?></td>
				<td><?php echo $row["betalt"]; ?></td>
				<td><?php echo $row["resterende"]; ?></td>
				<td><?php echo $row["betalingsmetode"]; ?></td>
			</tr>
			<?php
			$n++;
		}
		?>
			<tr><td colspan="10"><?php echo $pagination; ?></td></tr>
		<?php
		exit();
	}
}

//pagination og visning av ORDRE rader
if (isset($_POST["visOrdreDetaljer"])) {
	$administrer = new Administrer();
	$result = $administrer->endreRaderPagination("ordredetaljer", $_POST["pageno"]);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if (count($rows) > 0) {
		$n = (($_POST["pageno"] - 1) * 5) + 1;
		foreach ($rows as $row) {
			?>
			<tr>
				<td><?php echo $n; ?></td>
				<td><?php echo $row["ordreNr"]; ?></td>
				<td><?php echo $row["produktnavn"]; ?></td>
				<td><?php echo $row["antall"]; ?></td>
				<td><?php echo $row["pris"]; ?></td>
			</tr>
			<?php
			$n++;
		}
		?>
			<tr><td colspan="5"><?php echo $pagination; ?></td></tr>
		<?php
		exit();
	}
}

?>
