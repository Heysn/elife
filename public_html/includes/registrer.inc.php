<?php

class database {

  private $conn;

  public function connect() {
    include_once './db/constants.php';
    $this->conn = new mysqli(host, user, pass, db);
    if ($this->conn) {
      return $this->conn;
    }
    return "database forbindelse feilet";

  }
}

?>
