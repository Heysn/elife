<?php

class Administrer {

  private $conn;

  function __construct() {
    include_once '../db/db.php';
    $db = new Database();
    $this->conn = $db->connect();
  }

  public function endreRaderPagination($table, $pno) {
    $a = $this->pagination($this->conn, $table, $pno, 5);
    if ($table == "kategori") {
      $stmt = "SELECT `kategoriID`, `kategorinavn`, `status` FROM `kategori`".$a["limit"];
    } else if($table == "produkt") {
      $stmt = "SELECT p.produktID,k.kategorinavn,p.produktnavn,p.merke,p.beskrivelse,p.pris,p.antall,p.datoLagtTil,p.status
                FROM produkt p, kategori k WHERE p.kategoriID = k.kategoriID ".$a["limit"];
    } else if($table == "kunde") {
      $stmt = "SELECT k.kundeID,b.brukernavn,k.fornavn,k.etternavn,k.adresse,k.postNr,p.poststed,k.telefon
                FROM kunde k, bruker b, poststed p WHERE k.brukerID = b.brukerID AND k.postNr = p.postNr ".$a["limit"];
    } else {
      $stmt = "SELECT * FROM ".$table." ".$a["limit"];
    }
    $result = $this->conn->query($stmt) or die ($this->conn->error);
    $rows = array();
    if ($result->num_rows > 0) {
      //returnerer assosiativ array
      while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
      }
    }
    //returnerer pagination og array
    return ["rows"=>$rows,"pagination"=>$a["pagination"]];
  }

  private function pagination($conn,$table,$pno,$n){
		$query = $conn->query("SELECT COUNT(*) as rows FROM ".$table);
		$row = mysqli_fetch_assoc($query);
		//$totalRecords = 100000;
		$pageno = $pno;
		$numberOfRecordsPerPage = $n;

		$last = ceil($row["rows"]/$numberOfRecordsPerPage);

		$pagination = "<ul class='pagination'>";

		if ($last != 1) {
			if ($pageno > 1) {
				$previous = "";
				$previous = $pageno - 1;
				$pagination .= "<li class='page-item'><a class='page-link' pn='".$previous."' href='#' style='color:#333;'> Previous </a></li></li>";
			}
			for($i=$pageno - 5;$i< $pageno ;$i++){
				if ($i > 0) {
					$pagination .= "<li class='page-item'><a class='page-link' pn='".$i."' href='#'> ".$i." </a></li>";
				}

			}
			$pagination .= "<li class='page-item'><a class='page-link' pn='".$pageno."' href='#' style='color:#333;'> $pageno </a></li>";
			for ($i=$pageno + 1; $i <= $last; $i++) {
				$pagination .= "<li class='page-item'><a class='page-link' pn='".$i."' href='#'> ".$i." </a></li>";
				if ($i > $pageno + 4) {
					break;
				}
			}
			if ($last > $pageno) {
				$next = $pageno + 1;
				$pagination .= "<li class='page-item'><a class='page-link' pn='".$next."' href='#' style='color:#333;'> Next </a></li></ul>";
			}
		}
	//LIMIT 0,10
		//LIMIT 20,10
		$limit = "LIMIT ".($pageno - 1) * $numberOfRecordsPerPage.",".$numberOfRecordsPerPage;

		return ["pagination"=>$pagination,"limit"=>$limit];
	}

  public function slettRad($table,$pk,$id){
    if($table == "kategori"){
      $stmt = $this->conn->prepare("SELECT ".$id." FROM produkt WHERE kategoriID = ?");
      $stmt->bind_param("i",$id);
      $stmt->execute();
      $result = $stmt->get_result() or die($this->conn->error);
      if ($result->num_rows > 0) {
        return "Kan ikke slettes. Et eller flere produkter er avhengige av denne kategorien.";
      }else{
        $stmt = $this->conn->prepare("DELETE FROM ".$table." WHERE ".$pk." = ?");
        $stmt->bind_param("i",$id);
        $result = $stmt->execute() or die($this->conn->error);
        if ($result) {
          return "Kategori slettet.";
        }//kode for kategori da et produkt kan være avhengig av en kategori
      }
    }else{
      $stmt = $this->conn->prepare("DELETE FROM ".$table." WHERE ".$pk." = ?");
      $stmt->bind_param("i",$id);
      $result = $stmt->execute() or die($this->conn->error);
        if ($result) {
          return "Rad slettet.";
      }
    }//kode for slettinger i andre tabeller
  }

  //funksjon for å ta en spesifikk rad når du trykker på rediger-knappen i en administreringsside
  public function getEnRad($table,$pk,$id) {
    $stmt = $this->conn->prepare("SELECT * FROM ".$table." WHERE ".$pk."= ? LIMIT 1");
    $stmt->bind_param("i", $id);
    $stmt->execute() or die ($this->conn->error);
    $result = $stmt->get_result();
    if ($result->num_rows == 1) {
      $row = $result->fetch_assoc();
    }
    return $row;
  }

  //en metode for å oppdatere/redigere alle tabeller, isteden for 3 ulike metoder for 3 ulike tabeller
  //vi bare kaller funksjonen og pass'er nødvendige conditions og input ettersom hvilken tabell/informasjon vi oppererer med
  //where-condition pass'es i form av en array. samme gjelder $fields
  public function redigerRad($table,$where,$fields) {
    $sql = "";
    $condition = "";
    foreach ($where as $key => $value) {
      $condition .= $key. "='" .$value. "' AND ";
    }
    $condition = substr($condition, 0, -5);
    foreach ($fields as $key => $value) {
      $sql .= $key. "='" .$value. "', ";
    }
    $sql = substr($sql, 0, -2);
    $sql = " UPDATE " .$table. " SET " .$sql. " WHERE " .$condition;
    if(mysqli_query($this->conn,$sql)) {
      return "Raden er redigert.";
    }
  }

  public function lagreKundeOrdre($salgsDato, $kundeTlf, $bTotal, $mva, $nTotal, $betalt, $rst, $mtd, $lst_prNavn, $lst_antall, $lst_pris, $lst_antallLager) {
      $stmt = $this->conn->prepare("SELECT * FROM kunde WHERE telefon = ?");
      $stmt->bind_param("s",$kundeTlf);
      $stmt->execute();
      $result = $stmt->get_result() or die($this->conn->error);
      if ($result->num_rows < 1) {
        return "Telefonnummer ikke registrert.";
      } else {
        $stmt = $this->conn->prepare("INSERT INTO `ordre`(`ordreDato`, `telefon`, `bruttoTotal`, `mva`, `nettoTotal`, `betalt`, `resterende`, `betalingsmetode`)
        VALUES (?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssddddds", $salgsDato, $kundeTlf, $bTotal, $mva, $nTotal, $betalt, $rst, $mtd);
        $result = $stmt->execute() or die ($this->conn->error);
        $ordreNr = $stmt->insert_id;
        if(!$result) {
          return "INSERT ordre error.";
        }
        else {
        if ($ordreNr != null) {
          for ($i = 0; $i < count($lst_prNavn) ; $i++) {

            //resterende antall av et produkt etter registrering av et salg
            $rest_lager = $lst_antallLager[$i] - $lst_antall[$i];
            if ($rest_lager < 0) {
              return "Registrering av salget misslykkes. restLager er null.";
            } else {
              $stmt = "UPDATE produkt SET antall = '$rest_lager' WHERE produktnavn = '".$lst_prNavn[$i]."'";
              $this->conn->query($stmt);
            }

            $insertOrdreDtlj = $this->conn->prepare("INSERT INTO `ordredetaljer`(`ordreNr`, `produktnavn`, `antall`, `pris`)
            VALUES (?,?,?,?)");
            $insertOrdreDtlj->bind_param("isid", $ordreNr, $lst_prNavn[$i], $lst_antall[$i], $lst_pris[$i]);
            $insertOrdreDtlj->execute() or die ($this->conn->error);
          }
          return "Salget er registrert.";

        }
      }
    }
  }

}

//$obj = new Administrer();
//echo $obj->redigerRad("kategori", ["kategoriID"=>9], ["kategorinavn"=>"låsen", "status"=>1]);
//echo $obj->redigerRad("produkt", ["produktID"=>4], ["kategoriID"=>10, "produktnavn"=>"testen", "merke"=>"eeelife", "beskrivelse"=>"noe", "pris"=>"200", "antall"=>"2", "datoLagtTil"=>"2018-05-12", "status"=>1]);
//echo "<pre>";
//print_r($obj->endreRaderPagination("kategori",1));
//echo $obj->slettRad("kategori","kategoriID",2);
//print_r($obj->getEnRad("kategori","kategoriID",1));
 ?>
