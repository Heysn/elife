<?php
  include_once './db/constants.php';
  if (isset($_SESSION['b_id'])) {
    session_destroy();
  }

  header("location:".DOMAIN."/");

  ?>
