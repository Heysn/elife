<?php
  include_once './db/constants.php';
  if (!isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/");
  }
 ?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="./js/administrer.js"></script>
</head>
<body>

    <!--navbar-->
    <?php include_once './templates/header.php'; ?>
    <br/><br/>
    <div class="container">
      <table class="table table-hover table-bordered">
        <thead>
          <tr>
            <th>Kundenummer</th>
            <th>Bruker</th>
            <th>Fornavn</th>
            <th>Etternavn</th>
            <th>Adresse</th>
            <th>Postnummer</th>
            <th>Poststed</th>
            <th>Telefon</th>
            <th>Handling</th>
          </tr>
        </thead>
        <tbody id="get_kunde">

          <!--<tr>
            <td>1</td>
            <td>Anders</td>
            <td>Andersen</td>
            <td>Hyllveien 12 C</td>
            <td>3408</td>
            <td>46695561</td>
            <td>
              <a href="#" class="btn btn-danger btn-sm">Slett</a>
              <a href="#" class="btn btn-info btn-sm">Rediger</a>
            </td>
          </tr>-->

          <tr>
        </tbody>
      </table>
    </div>

    <?php include_once("./templates/rediger_kunde.php") ?>

</body>
</html>
