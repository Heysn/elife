<?php
  include_once './db/constants.php';
  if (!isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/");
  }
 ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="./js/salg.js"></script>
</head>
<body>
  <div class="overlay"><div class="loader"></div></div>
      <!--navbar-->
    <?php include_once("./templates/header.php"); ?>
    <br/><br/>

      <div class="containter">
        <div class="row" style="margin-right: 0px; margin-left: 0px;">
          <div class="col-md-10 mx-auto">
            <div class="card" style="box-shadow:0 0 10px 0 lightgrey;">
              <div class="card-header">
                <h4>Nytt Salg</h4>
              </div>
              <div class="card-body">
                <form id="salgs_form" onsubmit="return false">
                  <div class="form-group row">
    				  			<label class="col-sm-3" align="right">Ordre Dato</label>
    				  			<div class="col-sm-6">
    				  				<input type="text" id="salgsDato" name="salgsDato" readonly class="form-control form-control-sm" value="<?php echo date("Y-m-d h:m:s"); ?>">
    				  			</div>
    				  		</div>
                  <div class="form-group row">
                    <label class="col-sm-3" align="right">Telefon*</label>
                    <div class="col-sm-6">
                      <input type="text" id="kundeTlf" name="kundeTlf" class="form-control form-control-sm" placeholder="Skriv kundens telefonnummer">
                      <small id="t_error" class="form-text text-muted"></small>
                    </div>
                  </div>

                  <div class="card" style="box-shadow:0 0 5px 0 lightgrey;">
                    <div class="card-body">
                      <h3>Ordreliste</h3>
                      <table align="center" style="width:800px;">
                        <thead>
    		                  <tr>
    		                    <th>#</th>
    		                    <th style="text-align:center;">Produktnavn</th>
    		                    <th style="text-align:center;">På Lager</th>
    		                    <th style="text-align:center;">Antall</th>
    		                    <th style="text-align:center;">Kr Pris Pr</th>
    		                    <th style="text-align:center;">Totalt</th>
    		                  </tr>
    		                </thead>
    		                <tbody id="salg_element">
                          <!--<tr>
                              <td><b id="nummer">1</b></td>
                              <td>
                                <select name="produktid[]" class="form-control form-control-sm" required>
                                  <option>Gazelle</option>
                                </select>
                              </td>
                              <td><input name="antallLager[]" readonly type="text" class="form-control form-control-sm"></td>
                              <td><input name="antall[]" type="text" class="form-control form-control-sm" required></td>
                              <td><input name="pris[]" type="text" class="form-control form-control-sm" readonly></td>
                              <td>Kr 1540</td>
                          </tr>-->
    		                </tbody>
                      </table> <!--tabell slutter her-->
                      <center style="padding:10px;">
                        <button id="leggTil" style="width:85px;" class="btn btn-success">Legg Til</button>
                        <button id="fjern" style="width:85px;" class="btn btn-danger">Fjern</button>
                      </center>
                    </div> <!--nytt salg card body-->
                  </div> <!--ordreliste indre card body-->
                  <p></p>
                    <div class="form-group row">
                      <label for="bruttoTotal" class="col-sm-3 col-form-label" align="right">Pris ink.Mva.</label>
                      <div class="col-sm-6">
                        <input type="text" readonly name="bruttoTotal" class="form-control form-control-sm" id="bruttoTotal" required/>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="mva" class="col-sm-3 col-form-label" align="right">Mva. (25%)</label>
                      <div class="col-sm-6">
                        <input type="text" readonly name="mva" class="form-control form-control-sm" id="mva" required/>
                      </div>
                    </div>
                    <!--<div class="form-group row">
                      <label for="rabatt" class="col-sm-3 col-form-label" align="right">Rabatt %</label>
                      <div class="col-sm-6">
                        <input type="text" name="rabatt" class="form-control form-control-sm" id="rabatt" required/>
                        <input type="text" name="rabattTrekk" class="form-control form-control-sm" id="rabattTrekk"/>
                      </div>
                    </div>-->
                    <div class="form-group row">
                      <label for="nettoTotal" class="col-sm-3 col-form-label" align="right">Pris eks.Mva.</label>
                      <div class="col-sm-6">
                        <input type="text" readonly name="nettoTotal" class="form-control form-control-sm" id="nettoTotal" required/>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="betalt" class="col-sm-3 col-form-label" align="right">Betalt Kr</label>
                      <div class="col-sm-6">
                        <input type="text" name="betalt" class="form-control form-control-sm" id="betalt">
                        <small id="b_error" class="form-text text-muted"></small>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="resterende" class="col-sm-3 col-form-label" align="right">Resterende Beløp Kr</label>
                      <div class="col-sm-6">
                        <input type="text" readonly name="resterende" class="form-control form-control-sm" id="resterende" required/>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="betalingsMtd" class="col-sm-3 col-form-label" align="right">Betalingsmetode</label>
                      <div class="col-sm-6">
                        <select name="betalingsMtd" class="form-control form-control-sm" id="betalingsMtd" required/>
                          <option>Kontant</option>
                          <option>Kort</option>
                          <option>Sjekk</option>
                        </select>
                      </div>
                    </div>
                    <center>
                      <input type="submit" id="salgs_form_lagre" style="width:150px;" class="btn btn-info" value="Fullfør" />
                      <input type="submit" id="print_salg" style="width:150px;" class="btn btn-success d-none" value="Skriv Ut" />
                    </center>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

</body>
</html>
