<!DOCTYPE HTML>
// filen blir ikke brukt
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="./js/main.js"></script>
</head>
<body>
      <!--navbar-->
    <?php include_once './templates/header.php'; ?>
    <br/><br/>
    <div class="container">
      <div class="card mx-auto" style="width: 30rem;">
            <div class="card-header">Registrer</div>
            <div class="card-body">
              <form id="registreringsform" onsubmit="return false" autocomplete="off">
                <div class="form-group">
                  <label for="brukernavn">Brukernavn</label>
                  <input type="text" name="brukernavn" class="form-control" id="brukernavn" placeholder="">
                  <small id="u_error" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="fornavn">Fornavn</label>
                  <input type="text" name="fornavn" class="form-control" id="fornavn" placeholder="Ola">
                  <small id="u_error" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="etternavn">Etternavn</label>
                  <input type="text" name="etternavn" class="form-control" id="etternavn" placeholder="Nordmann">
                  <small id="u_error" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="passord1">Passord</label>
                  <input type="password" name="passord1" class="form-control"  id="passord1" placeholder="">
                  <small id="p1_error" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="passord2">Skriv passord på nytt</label>
                  <input type="password" name="passord2" class="form-control"  id="passord2" placeholder="">
                  <small id="p2_error" class="form-text text-muted"></small>
                </div>
                <button type="submit" name="registrer_btn" class="btn btn-primary"><span class="fa fa-user"></span>&nbsp;Registrer</button>
                <span><a href="index.php">Login</a></span>
              </form>
            </div>
          <div class="card-footer text-muted">

          </div>
        </div>
    </div>

</body>
</html>
