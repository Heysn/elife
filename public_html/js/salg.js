$(document).ready(function(){
	var DOMAIN = "http://localhost/elife/public_html";


	//kaller funksjon nyVareRad ved klikk på knappen med id="leggTil" i salg.php linje 77
	nyVareRad();
	$("#leggTil").click(function() {
		nyVareRad();
	})

	//funksjon som henter data som respons fra php-script i prosessering.inc.php fra getNyVare, som igjen bruker en funskjon getRader() fra
	//DbO.inc.php som henter alle rader fra en tabell du gir som parameter i funksjonen getNyVare
	function nyVareRad() {
		$.ajax({
			url : DOMAIN+"/includes/prosessering.inc.php",
			method : "POST",
			data : {getNyVare:1},
			success : function(data){
				$("#salg_element").append(data);
				var n = 0;
				//loop for første kolonne # for å vise nummerering på radene
				$(".nummer").each(function() {
					$(this).html(++n);
				})
			}
		})
	}

	//fjerner rad ved klikk på knappen med id="fjern" i salg.php linje 78
	//bruker children for å fjerne tablerow/tr i tbody "salg_element" i salg.php, og fjerner siste rad
	$("#fjern").click(function() {
		$("#salg_element").children("tr:last").remove();
		prisUtregninger(0);
	})

	//salg.js sender request og mottar pris og antall om et produkt i tabellen
	//med change som event
	$("#salg_element").delegate(".produktID", "change", function() {
		var produktID = $(this).val();
		var tr = $(this).parent().parent();
		$(".overlay").show();
		$.ajax({
			url : DOMAIN+"/includes/prosessering.inc.php",
			method : "POST",
			dataType : "json",
			data : {getPrisAntall:1, id:produktID},
			success : function(data){
				tr.find(".antallLager").val(data["antall"]);
				tr.find(".antall").val();
				tr.find(".pris").val(data["pris"]);
				tr.find(".prNavn").val(data["produktnavn"]);
				tr.find(".total").html( tr.find(".antall").val() * tr.find(".pris").val() );
				//kaller prisUtregninger med null som parameter for rabatt
				prisUtregninger(0);
			}
		})
	})

	//når man trykker på antall inputten kjøres denne funksjonen
	$("#salg_element").delegate(".antall", "keyup", function() {
		var antall = $(this);
		var tr = $(this).parent().parent();
		//bruker isNotaNumber for å se om bruker skriver inn en gyldig input i antall-input
		if (isNaN(antall.val())) {
			alert("Skriv en gyldig verdi.");
			antall.val("");
		} else {
			// - 0 for at den får med seg alle tallene en skriver inn.
			// uten - 0 får man ingen melding selv om du skriver 12 i antall-inputt og det er 2 produkter i databasen
			// (den forstår inputten som string og ikke tallverdier uten - 0)
			if ((antall.val() - 0) > (tr.find(".antallLager").val() - 0)) {
				alert("Det oppgitte antallet er ikke tilgjengelig for dette produktet.");
				antall.val("");
			} else {
				//ganger antall med pris for en rad/et produkt og viser resultatet under "totalt"
				tr.find(".total").html(antall.val() * tr.find(".pris").val());
				//kaller prisUtregninger med null som parameter for rabatt
				prisUtregninger(0);
			}
		}
	})

	function prisUtregninger(btlt) {
		var bruttoTotal = 0;
		var mva = 0;
		//var rabatt = rbt;
		var nettoTotal = 0;
		var betalt = btlt;
		var resterende = 0;
		$(".total").each(function() {
			//ganger med 1 fordi vi bruker .html isteden for .val
			//sørger for at verdien oppfører seg som en tallverdi og ikke string
			bruttoTotal = bruttoTotal + ($(this).html() * 1);
		})
		mva = 0.25 * bruttoTotal;
		nettoTotal = bruttoTotal - mva;
		//nettoTotal = nettoTotal - rabatt;
		resterende = bruttoTotal - betalt;
		$("#bruttoTotal").val(bruttoTotal);
		$("#mva").val(mva);
		//$("#rabatt").val(rabatt);
		$("#nettoTotal").val(nettoTotal);
		$("#resterende").val(resterende);
		//$("#betalt")
	}

	//$("#rabatt").keyup(function() {
		 //var rabatt = $(this).val();
		 //prisUtregninger(rabatt, 0);
	//})

	$("#betalt").keyup(function() {
		var betalt = $(this).val();
		//var rabatt = $("#rabatt").val();
		prisUtregninger(betalt);
	})

	//-----------Lagre et salg---------------
//	$("#salgs_form_lagre").click(function() {
	//	var salg = $("#salgs_form").serialize();

		//$.ajax({
			//url : DOMAIN+"/includes/prosessering.inc.php",
			//method : "POST",
			//data : $("#salgs_form").serialize(),
			//success : function(data) {
				//$("#salgs_form").trigger("reset");
				//alert(data);
			//}
		//})
	//})

	$("#salgs_form_lagre").click(function() {
		var tlf = $("#kundeTlf");
		var betalt = $("#betalt");
		var status = false;
		if (tlf.val() == "") {
			tlf.addClass("border-danger");
			$("#t_error").html("<span class='text-danger'>Felt kan ikke være tomt.</span>");
			status = false;
		} else {
			tlf.removeClass("border-danger");
			$("#t_error").html("");
			status = true;
		}
		if (betalt.val() == "") {
			betalt.addClass("border-danger");
			$("#b_error").html("<span class='text-danger'>Felt kan ikke være tomt.</span>");
			status = false;
		} else {
			betalt.removeClass("border-danger");
			$("#b_error").html("");
			status = true;
		}
		if (status) {
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : $("#salgs_form").serialize(),
				success : function(data) {
					if (data == "Telefonnummer ikke registrert.") {
						alert("Salget kunne ikke gjennomføres. Ingen kunder er registrert på det oppgitte telefonnummeret.");
					} else if (data == "Salget er registrert.") {
            alert(data);
						$("#salgs_form").trigger("reset");
					} else {
					alert("Salget kunne ikke gjennomføres.");
					alert(data);
				}
				}
			})
		}
	})

});
