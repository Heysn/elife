$(document).ready(function(){
	var DOMAIN = "http://localhost/elife/public_html";

  //Login
  $("#form_login").on("submit",function() {
    var brukernavn = $("#log_bruker");
    var pass = $("#log_pass");
    var login = $("#log_error");
    var status = false;
    if (brukernavn.val() == "") {
      brukernavn.addClass("border-danger");
      $("#b_error").html("<span class='text-danger'>Felt kan ikke være tomt.</span>");
      status = false;
    } else {
      brukernavn.removeClass("border-danger");
      $("#b_error").html("");
      status = true;
    }
    if (pass.val() == "") {
      pass.addClass("border-danger");
      $("#p_error").html("<span class='text-danger'>Felt kan ikke være tomt.</span>");
      status = false;
    } else {
      pass.removeClass("border-danger");
      $("#p_error").html("");
      status = true;
    }
    if (status) {
			$(".overlay").show();
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : $("#form_login").serialize(),
				success : function(data) {
					if (data == "Feil ved innlogging1.") {
						$(".overlay").hide();
						//login.addClass("border-danger");
						$("#log_error").html('<div class="alert alert-danger"><span class="text-danger">Autentisering feilet.</span>');
					}else if(data == "Feil ved innlogging2.") {
						$(".overlay").hide();
						//login.addClass("border-danger");
						$("#log_error").html('<div class="alert alert-danger"><span class="text-danger">Autentisering feilet.</span>');
						status = false;
					}else{
						$(".overlay").hide();
						console.log(data);
						window.location.href = DOMAIN+"/velkommen.php";
					}
				}
			})
		}
  })

	//henter kategorier som er blitt lagt til. funkasjonen kan kalles og
	//select-listen(e) vil være oppdatert med en gang
	hentKategori();
	function hentKategori(){
		$.ajax({
			url : DOMAIN+"/includes/prosessering.inc.php",
			method : "POST",
			data : {getKategorier:1},
			success : function(data){
				//alert(data);
				var velg = "<option value=''>Velg Kategori</option>";
				$("#velgKategori").html(velg+data);
			}
		})
	}

	//legger til kategori
	$("#kategori_form").on("submit",function(){
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : $("#kategori_form").serialize(),
				success : function(data) {
					if (data == "Kategori lagt til.") {
						alert("Ny kategori er blitt lagt til.");
						$("#kategorinavn").val("");
						hentKategori();
					} else {
						alert(data);
					}
				}
			})
	})

	//SCUFFED AJAX legger til produkt
	$("#produkt_form").on("submit",function() {
		$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : $("#produkt_form").serialize(),
				success : function(data) {
					if (data == "Produkt lagt til.") {
						alert("Nytt produkt er blitt lagt til.");
						$("#produktnavn").val("");
						$("#velgKategori").val("");
						$("#merke").val("");
						$("#pris").val("");
						$("#antall").val("");
						$("#beskrivelse").val("");
					}else{
						console.log(data);
						alert(data);
					}

				}
			})
	})

	//SCUFFED AJAX legger til kunde
	$("#kunde_form").on("submit",function() {
		$.ajax({
			url : DOMAIN+"/includes/prosessering.inc.php",
			method : "POST",
			data : $("#kunde_form").serialize(),
			success : function(data) {
				if (data == "Kunde lagt til.") {
					alert("Ny kunde er blitt lagt til.");
					$("#fornavn").val("");
					$("#etternavn").val("");
					$("#adresse").val("");
					$("#postNr").val("");
					$("#telefon").val("");
				} else {
					console.log(data);
					alert(data);
				}
			}
		})
	})

})
