$(document).ready(function(){
	var DOMAIN = "http://localhost/elife/public_html";

  //----------administrer kategorier-------------

  //administrer kategorier
  visKategorier(1);
  function visKategorier(pn) {
    $.ajax({
      url : DOMAIN+"/includes/prosessering.inc.php",
      method : "POST",
      data : {visKategorier:1,pageno:pn},
      success : function(data) {
        $("#get_kategori").html(data);
      }
    })
  }

  //klassene page-link på "click" event kjører en funksjon
  //får instance når noen trykker på en sidelink
  //kaller endrekategori og paser pn for å navigere sidenummer
  $("body").delegate(".page-link", "click", function() {
    var pn = $(this).attr("pn");
    visKategorier(pn);
  })

  //trenger bare å kalle alle funksjoner her.
  //alt forarbeid er allerede gjort i de første 72 linjene i "endre.php"
  //allerede returnert funksjon endreRaderPagination
  //vi trenger bare å pass'e navn på tabeller ved "$table". f.eks. har vi pass'a "kategori" tabellen
  //og returnert rader med pagination på en grense med 5 rader per side/page

	$("body").delegate(".slettKategori", "click", function() {
		//var did tar instans av klassen "slettKategori" i fila "prosessering.php"
		//ettersom hvilken klasse/knapp du trykker på, får du instansen av den klassen, ved bruk av this/dette nøkkelordet "did"
    //jquery funksjon "attr" for å få atributtet, hvilket er defintert som "did" under.
		var did = $(this).attr("did");
    //confirm for å få bekreftelse fra bruker før sletting
    if (confirm("Vil du slette denne kategorien?")) {
      $.ajax({
        url : DOMAIN+"/includes/prosessering.inc.php",
        method : "POST",
        data : {slettKategori:1, id:did},
        success : function(data) {
          //data er bare respons fra php script
          if (data == "Kan ikke slettes. Et eller flere produkter er avhengige av denne kategorien.") {
            alert("ERROR: Et produkt avhenger av denne kategorien og kan ikke slettes.");
          } else if (data == "Kategori slettet.") {
              alert("Kategorien er slettet.");
              visKategorier(1);
          } else if (data == "Rad slettet.") {
              alert("Kategorien er slettet.");
          } else {
            alert(data);
          }

        }
      })
    } else {

    }
	})

	//redigerer en rad du trykker på i kategori
	$("body").delegate(".redigerKategori", "click", function() {
		var eid = $(this).attr("eid");
		$.ajax({
      url : DOMAIN+"/includes/prosessering.inc.php",
      method : "POST",
      dataType : "json",
      data : {redigerKategori:1, id:eid},
      success : function(data){
        console.log(data);
        $("#kategoriID").val(data["kategoriID"]);
        $("#rediger_kategorinavn").val(data["kategorinavn"]);
      }
    })
	})

  //ajax for lagre-knapp i kategori administrering/redigering
  $("#rediger_kategori_form").on("submit", function() {
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : $("#rediger_kategori_form").serialize(),
				success : function(data) {
          if (data == "Raden er redigert.") {
            alert("Kategorien er redigert.");
            window.location.href = "";
          } else {
            alert(data);
          }
				}
			})
  })

  //----------administrer produkter-------------

  //henter kategorier som er blitt lagt til. funkasjonen kan kalles og
  //select-listen(e) vil være oppdatert med en gang
  hentKategori();
  function hentKategori(){
    $.ajax({
      url : DOMAIN+"/includes/prosessering.inc.php",
      method : "POST",
      data : {getKategorier:1},
      success : function(data){
        //alert(data);
        var velg = "<option value=''>Velg Kategori</option>";
        $("#velgKategori").html(velg+data);
      }
    })
  }

	//produkt-administrer
	visProdukter(1);
	function visProdukter(pn) {
		$.ajax({
			url : DOMAIN+"/includes/prosessering.inc.php",
			method : "POST",
			data : {visProdukter:1,pageno:pn},
			success : function(data) {
				$("#get_produkt").html(data);
			}
		})
	}
	$("body").delegate(".page-link", "click", function() {
		var pn = $(this).attr("pn");
		visProdukter(pn);
	})

  //slett produkt akkurat som slett kategori over
  $("body").delegate(".slettProdukt", "click", function() {
    var did = $(this).attr("did");
    //confirm for å få bekreftelse fra bruker før sletting
    if (confirm("Vil du slette dette produktet?")) {
      $.ajax({
        url : DOMAIN+"/includes/prosessering.inc.php",
        method : "POST",
        data : {slettProdukt:1, id:did},
        success : function(data) {
          //data er bare respons fra php script
          if (data == "Rad slettet.") {
            alert("Produktet er slettet.");
            visProdukter(1);
          } else {
            alert(data);
          }
        }
      })
    }
  })

  //rediger produkt
  $("body").delegate(".redigerProdukt","click",function() {
    var eid = $(this).attr("eid");
    $.ajax({
      url : DOMAIN+"/includes/prosessering.inc.php",
      method : "POST",
      dataType : "json",
      data : {redigerProdukt:1,id:eid},
      success : function(data){
        console.log(data);
        $("#produktID").val(data["produktID"]);
        $("#velgKategori").val(data["kategoriID"]);
        $("#rediger_produktnavn").val(data["produktnavn"]);
        $("#merke").val(data["merke"]);
        $("#beskrivelse").val(data["beskrivelse"]);
        $("#pris").val(data["pris"]);
        $("#antall").val(data["antall"]);
      }
    })
  })

  //rediger produkt
  $("#rediger_produkt_form").on("submit",function() {
    $.ajax({
        url : DOMAIN+"/includes/prosessering.inc.php",
        method : "POST",
        data : $("#rediger_produkt_form").serialize(),
        success : function(data){
          if (data == "Raden er redigert.") {
            alert("Produktet er redigert.");
            window.location.href = "";
          } else {
            alert(data);
          }
        }
      })
  })

	  //----------administrer kunder-------------
		//kunde-administrer
		visKunder(1);
		function visKunder(pn) {
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : {visKunder:1,pageno:pn},
				success : function(data) {
					$("#get_kunde").html(data);
				}
			})
		}
		$("body").delegate(".page-link", "click", function() {
			var pn = $(this).attr("pn");
			visKunder(pn);
		})

    //slett kunde akkurat som slett kategori/produkt over
    $("body").delegate(".slettKunde", "click", function() {
      var did = $(this).attr("did");
      //confirm for å få bekreftelse fra bruker før sletting
      if (confirm("Vil du slette denne kunden?")) {
        $.ajax({
          url : DOMAIN+"/includes/prosessering.inc.php",
          method : "POST",
          data : {slettKunde:1, id:did},
          success : function(data) {
            //data er bare respons fra php script
            if (data == "Rad slettet.") {
              alert("Kunden er slettet.");
              visKunder(1);
            } else {
              alert(data);
            }
          }
        })
      }
    })

    //rediger kunde
    $("body").delegate(".redigerKunde","click",function() {
      var eid = $(this).attr("eid");
      $.ajax({
        url : DOMAIN+"/includes/prosessering.inc.php",
        method : "POST",
        dataType : "json",
        data : {redigerKunde:1,id:eid},
        success : function(data){
          console.log(data);
          $("#kundeID").val(data["kundeID"]);
          $("#rediger_fornavn").val(data["fornavn"]);
          $("#etternavn").val(data["etternavn"]);
          $("#adresse").val(data["adresse"]);
          $("#postNr").val(data["postNr"]);
          $("#pris").val(data["pris"]);
          $("#telefon").val(data["telefon"]);
        }
      })
    })

    //rediger kunde
    $("#rediger_kunde_form").on("submit",function() {
      $.ajax({
          url : DOMAIN+"/includes/prosessering.inc.php",
          method : "POST",
          data : $("#rediger_kunde_form").serialize(),
          success : function(data){
            if (data == "Raden er redigert.") {
              alert("Kunden er redigert.");
              window.location.href = "";
            } else {
              alert("ERROR: Kunden kunne ikke redigeres. Pass på at telefonnummeret er unikt.");
            }
          }
        })
    })

    //--------------------VISNING AV SALG/ORDRE----------------------

		//ordre-oversikt
		visOrdre(1);
		function visOrdre(pn) {
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : {visOrdre:1,pageno:pn},
				success : function(data) {
					$("#get_salg_ordre").html(data);
				}
			})
		}
		$("body").delegate(".page-link", "click", function() {
			var pn = $(this).attr("pn");
			visOrdre(pn);
		})

    //ordre-oversikt
		visOrdreDetaljer(1);
		function visOrdreDetaljer(pn) {
			$.ajax({
				url : DOMAIN+"/includes/prosessering.inc.php",
				method : "POST",
				data : {visOrdreDetaljer:1,pageno:pn},
				success : function(data) {
					$("#get_salg_detaljer").html(data);
				}
			})
		}
		$("body").delegate(".page-link", "click", function() {
			var pn = $(this).attr("pn");
			visOrdreDetaljer(pn);
		})

})
