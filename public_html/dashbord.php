<?php
  include_once './db/constants.php';
  if (!isset($_SESSION['b_id'])) {
    header("location:".DOMAIN."/");
  }
 ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lagerstyring</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="./js/main.js"></script>
</head>
<body>

    <!--kategoriModal-->
    <?php include_once './templates/kategori.php'; ?>
    <!--produktModal-->
    <?php include_once './templates/produkt.php'; ?>
    <!--kundeModal-->
    <?php include_once './templates/kunde.php'; ?>
      <!--navbar-->
    <?php include_once './templates/header.php'; ?>
    <br/><br/>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="card mx-auto">
            <img class="card-img-top mx-auto" style="width:50%;" src="./images/userlogin.png" alt="Card image cap">
            <div class="card-body">
              <h4 class="card-title">Profil</h4>
              <p class="card-text"><i class="fa fa-user">&nbsp;</i>Brukernavn: <?php echo $_SESSION['b_navn']; ?> </p>
              <p class="card-text"><i class="fa fa-user">&nbsp;</i>Navn: <?php echo $_SESSION['b_etternavn'] ?>, <?php echo $_SESSION['b_fornavn']; ?> </p>
              <p class="card-text"><i class="fa fa-calendar">&nbsp;</i>Sist pålogget: <?php echo $_SESSION['s_login']; ?> </p>
              <p class="card-text"><i class="fa fa-envelope">&nbsp;</i>Email: <?php echo $_SESSION['b_email']; ?> </p>
              <!--<a href="#" class="btn btn-primary" style="background-color:#F2B33F; border-color: black; color:black">
                <i class="fa fa-edit" style="color:black">&nbsp;</i>Rediger Profil</a>-->
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="jumbotron" style="width:100%; height:100%;">
            <h1>Velkommen, <?php echo $_SESSION['b_fornavn']; ?>. Ha en fin dag!  </h1>
            <div class="row">
              <div class="col-sm-6">
                <iframe src="http://free.timeanddate.com/clock/i66zux8x/n1370/szw160/szh160/hoc000/hbw2/cf100/hnce1ead6"
                frameborder="0" width="160" height="160"></iframe>

              </div>
              <div class="col-sm-6">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Salg</h4>
                    <p class="card-text">Her kan du registrere salg og se gjennom detaljer.</p>
                    <a href="salg.php" class="btn btn-primary">Nytt salg</a>
                    <a href="oversikt_salg.php" class="btn btn-primary">Oversikt</a>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <p></p>
  <p></p>
  <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Kategorier</h4>
              <p class="card-text">Opprett og styr kategorier for dine produkter her.</p>
              <a href="#" data-toggle="modal" data-target="#kategoriModal" class="btn btn-primary">Legg til</a>
              <a href="administrer_kategori.php" class="btn btn-primary">Administrer</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Produkter</h4>
              <p class="card-text">Her kan du legge til og behandle dine produkter.</p>
              <a href="#" data-toggle="modal" data-target="#produktModal" class="btn btn-primary">Legg til</a>
              <a href="administrer_produkt.php" class="btn btn-primary">Administrer</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Kunder</h4>
              <p class="card-text">Legg til, oppdater og behandle dine kunders informasjon.</p>
              <a href="#" data-toggle="modal" data-target="#kundeModal" class="btn btn-primary">Legg til</a>
              <a href="administrer_kunder.php" class="btn btn-primary">Administrer</a>
            </div>
          </div>
        </div>
      </div>
    </div>

</body>
</html>
